<?php 

class M_matkul extends CI_model {
    public function getAllData()
    {
        return $this->db->get('mata_kuliah')->result_array();
    }

    public function tambahData($data)
    {
        // $data = [
        //     "mata_kuliah"   => $this->input->post('mata_kuliah', true),
        //     "semester"      => $this->input->post('semester', true)
        // ];

        $this->db->insert('mata_kuliah', $data);
    }

    public function hapusData($id)
    {
        // $this->db->where('id', $id);
        $this->db->delete('mata_kuliah', ['id' => $id]);
    }

    public function getDataById($id)
    {
        return $this->db->get_where('mata_kuliah', ['id' => $id])->row_array();
    }

    public function ubahData()
    {
        $data = [
            "mata_kuliah"   => $this->input->post('mata_kuliah', true),
            "semester"      => $this->input->post('semester', true),
            
        ];

        $this->db->where('id', $this->input->post('id'));
        $this->db->update('mata_kuliah', $data);
    }

    public function cariData()
    {
        $keyword = $this->input->post('keyword', true);
        $this->db->like('mata_kuliah', $keyword);
        $this->db->or_like('semester', $keyword);
        return $this->db->get('mata_kuliah')->result_array();
    }
}